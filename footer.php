				<div id="footer-wrapper" class="wow fadeIn">
					<div id="footer" class="container">
						<!-- header>
							<h2>Get in touch</h2>
						</header -->
						<div class="row">
							<div class="6u 12u(mobile)">
								<section>
									
                                    
                                    <form id="form1" name="form1" class="wufoo topLabel page" accept-charset="UTF-8" autocomplete="off" enctype="multipart/form-data" method="post" novalidate action="https://pdelsignore.wufoo.com/forms/z1gzmed106o7bdo/#public">
										<div id="foli3" class="notranslate c-name row 50%">
											<div class="6u 12u(mobile)">
												<input id="Field3" name="Field3" placeholder="Name" type="text" class="field text medium" maxlength="255" tabindex="1" onkeyup=""  />

											</div>
											<div id="foli4" class="notranslate c-email 6u 12u(mobile)">
												<input id="Field4" name="Field4" type="email" spellcheck="false" class="field text medium" placeholder="Email" tabindex="2" required />
											</div>
										</div>
										<div id="foli5" class="notranslate c-message row 50%">
											<div class="12u">
												<textarea  id="Field5" name="Field5" placeholder="Message" spellcheck="true"></textarea>
											</div>
										</div>
										<div class="row 50%">
											<div class="12u">
                                                <input id="saveForm" name="saveForm" class="btTxt submit" type="submit" value="Send" class="form-button-submit button icon fa-envelope" />

											</div>
										</div>
                                        <div class="hide">
<label for="comment">Do Not Fill This Out</label>
<textarea name="comment" id="comment" rows="1" cols="1"></textarea>
<input type="hidden" id="idstamp" name="idstamp" value="odMs1pIODyiQ34IcshZ6mmumQv4D6R/zA3icsaLotqU=" />
    </div>
									</form>
                                    



                                    
                                    
								</section>
							</div>
							<div class="6u 12u(mobile)">
								<section>
									<p>There are plenty of ways to get in touch with us. Feel free to send a message, call us, or reach out to any of the social apps listed below</p>
									<div class="row">
										<div class="6u 12u(mobile)">
											<ul class="icons">
												
												<li class="icon fa-phone">
													(973) 897-8086
												</li>
												<li class="icon fa-envelope">
													<a href="mailto:info@room1design.com">info@room1design.com</a>
												</li>
                                                <li class="icon fa-twitter">
													<a href="https://twitter.com/pdelsignore", target="_blank">@pdelsignore</a>
												</li>
                                                 <li class="icon fa-file">
													<a href="http://notes.pauldelsignore.com", target="_blank">Articles/Blog/Notes</a>
												</li>
											</ul>
										</div>
										<div class="6u 12u(mobile)">
											<ul class="icons">
                                                <li class="icon fa-codepen">
													<a href="http://codepen.io/pdelsignore/", target="_blank">codepen.io/pdelsignore/</a>
												</li>
												
												<li class="icon fa-instagram">
													<a href="https://www.instagram.com/pdelsignore/", target="_blank">instagram.com/pdelsignore</a>
												</li>
												
												<li class="icon fa-flickr">
													<a href="https://www.flickr.com/photos/pdelsignore", target="_blank">flickr.com/photos/pdelsignore</a>
												</li>
											</ul>
										</div>
									</div>
								</section>
							</div>
						</div>
					</div>
					<div id="copyright" class="container">
						<ul class="links">
							<li>&copy; Room 1 Design</li>
						</ul>
					</div>
				</div>