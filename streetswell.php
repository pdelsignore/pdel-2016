<!DOCTYPE HTML>

<html>
	<head>
		<?php include("head.php"); ?>
	</head>
	<body class="homepage">
		<div id="page-wrapper">

			<!-- Header -->
				<?php include("header.php"); ?>
            
            

			<!-- works -->
    <div class="project-header streetswell wow fadeIn">
        <div class="container">
                <h3 class="project-title"><span class="pr">Project:</span> Risbridger</h3>
                <img class="project-silo wow fadeIn" src="images/streetswell/streetswell-rwd.png" />
        </div>
    </div>
    <div class="project-content container wow fadeIn">
        <div class="content">
            <p>Street Swell creates and sells beautiful custom longboards and Mountain Bikes. I worked on the design of the site which runs responsively on all display devices. </p>
             <ul class="actions">
                            <li><a href="http://www.streetswell.com" target="_blank" class="button icon fa-file">View the Live Site</a></li>
                        </ul>
        </div>
            
    </div>

			<!-- Banner -->
				<?php include("about-banner.php"); ?>



			<!-- Footer -->
            <?php include("footer.php"); ?>

		</div><!-- page wrapper -->

		<!-- Scripts -->
			
    <?php include("end-scripts.php"); ?>

	</body>
</html>