<!DOCTYPE HTML>

<html>
	<head>
		<?php include("head.php"); ?>
	</head>
	<body class="homepage">
		<div id="page-wrapper">

			<!-- Header -->
				<?php include("header.php"); ?>
            
            

			<!-- works -->
    <div class="project-header risbridger wow fadeIn">
        <div class="container">
                <h3 class="project-title"><span class="pr">Project:</span> Risbridger</h3>
                <img class="project-silo wow fadeIn" src="images/risbridger/risbridger-rwd.png" />
        </div>
    </div>
    <div class="project-content container wow fadeIn">
        <div class="content">
            <p>Risdger designs and manufactures specialised engineering products for the international aircraft servicing and petro-chemical industries. Our task was to design and build a responsive website with a user-friendly method to update and manage content.   
                </p>
            <p>Project features include a robust product search, responsive images, carousel, and a notice board gallery</p>
            <p>Technologies include the Drupal CMS, the Omega base theme for visual theme configurations, Sass/css, and some custom module scripting. Room 1 Design did the design and custom theme work, as well as the content architecture. </p>
             <ul class="actions">
                            <li><a href="http://www.risbridger.com" target="_blank" class="button icon fa-file">View the Live Site</a></li>
                        </ul>
        </div>
            
    </div>

			<!-- Banner -->
				<?php include("about-banner.php"); ?>



			<!-- Footer -->
            <?php include("footer.php"); ?>

		</div><!-- page wrapper -->

		<!-- Scripts -->
			
    <?php include("end-scripts.php"); ?>

	</body>
</html>