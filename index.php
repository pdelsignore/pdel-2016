<!DOCTYPE HTML>

<html>
	<head>
		<?php include("head.php"); ?>
	</head>
	<body class="homepage">
		<div id="page-wrapper">

			<!-- Header -->
					<div id="header-wrapper">
					<div id="header" class="container wow fadeIn">

						<!-- Logo -->
							<h1 class="logo"><a href="index.php"><img src="images/r1-logo.png"/></a></h1>
							<p class="site-title">Room 1 Design Studio</p>

						<!-- Nav -->
							<nav>
                                <p class="main-menu"><a href="#" onclick="$('#banner-wrapper').animatescroll();">about</a> |  <a href="#" onclick="$('#features').animatescroll();">work</a></p>
                            </nav>

					</div>
				</div>
            
            

			<!-- Features -->
            <div id="features-wrapper">
                <section id="features" class="container">
						
						
							

                 <section>
                    <a href="risbridger.php" class="image featured"><img src="images/risbridger.jpg" alt="" /></a>
                        <header>
                            <h2>Risbridger</h2>
                        </header>

                        <p>Risbridger designs and manufactures specialised engineering products for the international aircraft servicing and petro-chemical industries. Our task was design and build a responsive website with a user friendly backend content management system
                            
                        <ul class="actions">
                            <li><a href="risbridger.php" class="button icon fa-file">View Project</a></li>
                        </ul>
                        </p>
                </section>
                
                
                <section>
                    <a href="streetswell.php" class="image featured"><img src="images/streetswell.jpg" alt="" /></a>
                        <header>
                            <h2>Street Swell</h2>
                        </header>

                        <p>Street Swell creates and sells beautiful custom longboards and Mountain Bikes.
                        <ul class="actions">
                            <li><a href="streetswell.php" class="button icon fa-file">View Project</a></li>
                        </ul>
                        </p>
                </section>

									
  
					</section> <!-- features container -->
            </div><!-- features wrapper -->

			<!-- Banner -->
				<?php include("about-banner.php"); ?>



			<!-- Footer -->
            <?php include("footer.php"); ?>

		</div><!-- page wrapper -->

		<!-- Scripts -->
			
    <?php include("end-scripts.php"); ?>

	</body>
</html>