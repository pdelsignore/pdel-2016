<div id="banner-wrapper" class="wow fadeIn">
					<div class="inner">
						<section id="banner" class="container">
							<p>Room 1 Design is Paul and Karin Del Signore, <br/> a small studio that specializes in designing and developing mobile/desktop experiences. </p>
                            <p>We build websites for small businesses, as well as partner with larger agencies on projects.</p>
						</section>
                        </div>
				</div>